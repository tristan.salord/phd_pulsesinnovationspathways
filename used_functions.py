## Needed libraries for function module
from itertools import combinations
import itertools
import json
import matplotlib
from matplotlib import pyplot as plt 
import numpy as np
import pandas as pd
from pandarallel import pandarallel
pandarallel.initialize(progress_bar=False)
import re
import requests
import spacy
from spacy.tokenizer import Tokenizer
from spacy.util import compile_prefix_regex,compile_suffix_regex,compile_infix_regex
from sklearn.feature_extraction.text import CountVectorizer
from spacy.matcher import Matcher
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import text
from sklearn.metrics.pairwise import cosine_similarity
from spacy.util import compile_prefix_regex,compile_suffix_regex,compile_infix_regex
import string
import time
## Text Manipulation functions
stop_words = text.ENGLISH_STOP_WORDS
punctuation=string.punctuation



def corpus_preprocess(dataframe):
    def tolist(rawstring):
        splitedstring=[z.strip() for z in rawstring.split(';')]
        return splitedstring
    workdf=dataframe.copy()
    
    ###### give right formatting for columns names
    if "Author/s" in workdf.columns:
        workdf=workdf.rename(columns={"Author/s": "authors"})
    if "Lens ID" in workdf.columns:
        workdf=workdf.rename(columns={"Lens ID": "lens_id"})
    if "Title" in workdf.columns:
        workdf=workdf.rename(columns={"Title": "title"})
    if "Abstract" in workdf.columns:
        workdf=workdf.rename(columns={"Abstract": "abstract"})
    if "References" in workdf.columns:
        workdf=workdf.rename(columns={"References": "references"})
    if "Date Published" in workdf.columns:
        workdf['year']=workdf.apply(lambda row: row['Date Published'].split('-')[0] if type(row['Date Published']) != float else row['Publication Year'],axis=1 )
        workdf['month']=workdf.apply(lambda row: row['Date Published'].split('-')[1] if type(row['Date Published']) != float else 1,axis=1)
        workdf['day']=workdf.apply(lambda row: row['Date Published'].split('-')[2] if type(row['Date Published']) != float else 1,axis=1)
        workdf=workdf.drop(columns = "Date Published")
    if "Publication Type" in workdf.columns:
        workdf=workdf.rename(columns={"Publication Type": "publication_type"})
    if "Citing Works Count" in workdf.columns:
        workdf=workdf.rename(columns={"Citing Works Count": "scholarly_citations_count"})
    if "DOI" in workdf.columns:
        workdf=workdf.rename(columns={"DOI": "doi"})
    if "Fields of Study" in workdf.columns:
        workdf=workdf.rename(columns={"Fields of Study": "fields"})
    if "Source Title" in workdf.columns:
        workdf=workdf.rename(columns={"Source Title": "source_title"})
    
    
        
    
    
 
    ###### concatenating title and abstract
    workdf['text_dat']=workdf.apply(lambda row: str(row['title']+'. '+str(row['abstract'])) if type(row['abstract']) != float else str(row['title']) ,axis=1)


    ###### splitting author list
    workdf['auth_list']=workdf['authors'].apply(lambda x: tolist(x) if type(x) != float else 0)
    ###### splitting ref list
    workdf=workdf[workdf['references'] != "n/a"]
    workdf['ref_list']=workdf['references'].apply(lambda x: tolist(x) if type(x) != float else 0)


    ###### dropping duplicated elements
    #workdf=workdf.drop_duplicates(subset='lens_id')
    workdf=workdf.sort_values(by=['lens_id','scholarly_citations_count'], ascending=[False,False]).drop_duplicates(['lens_id'], keep='first')
    workdf=workdf.drop(workdf[(workdf.title.isna()) & (workdf.abstract.isna())].index)
    workdf['title_clean']=workdf.title.str.lower().apply(lambda x: cleaner(x))
    workdf=workdf.sort_values(by=['title_clean','scholarly_citations_count'], ascending=[False,False]).drop_duplicates(['title_clean'], keep='first')
    
    ###### removing unwanted elements
    if 'level_0' in workdf.columns:
        workdf=workdf.drop(columns=['level_0'])
    elif 'index' in workdf.columns:
        workdf=workdf.drop(columns=['index'])
    
    workdf=workdf[workdf.scholarly_citations_count > 0]
    
    workdf=workdf[['lens_id', 'title', 'publication_type', 'abstract','text_dat','source_title',
       'year', 'month', 'day',
       'doi', 'authors','auth_list','fields', 'references','ref_list','scholarly_citations_count']]
    
    return workdf

        
def corpus_stat(dataframe,to_core=True,*args):
    
    seedsdic={'031-620-848-395-049':'''Why are institutions the ‘carriers of history’?: Path dependence and the evolution of conventions, organizations and institutions''',
          '138-335-719-527-313':'''Clio and the economics of QWERTY''',
          '078-540-302-576-45X':'''Competing Technologies, Increasing Returns, and Lock-In by Historical Events''',
          '157-501-463-801-357':'''Increasing Returns and Path Dependence in the Economy''',
          '003-422-869-824-612':'''The Fable of Keys''',
          '057-519-331-033-253':'''Path Dependence, Lock-In, and History''',
          '078-422-899-116-204':'''Sprayed to Death: Path Dependence, Lock-in and Pest Control Strategies''',
          '120-557-893-863-777':'''Nuclear Power Reactors: A Study in Technological Lock-in'''}
    workdf=dataframe.copy()
    
    #### Statistics on cited ref
    workdf['nb_ref']=workdf.ref_list.apply(lambda x: len(x))
    workdf['nb_cited_tf']=workdf.ref_list.apply(lambda x: len([ref for ref in x if ref in list(seedsdic.keys())]))
    workdf['tf_weight']=workdf.apply(lambda row: round(row['nb_cited_tf']/row['nb_ref'],3),axis=1)

    #### Comparison stats
    if args:
        compdf=args[0].copy()
        if to_core is True:
            workdf['nb_cited_core_seeds']=workdf.ref_list.apply(lambda x: len([z for z in x if z in compdf.lens_id.to_list() ]))
            workdf['lcs_core']=workdf.lens_id.apply(lambda x:len(compdf.loc[compdf.ref_list.apply(lambda z: x in z)]))
            workdf['spscore']=workdf.apply(lambda row: round(row['lcs_core']/row['scholarly_citations_count'],3),axis=1 )
            workdf=workdf[workdf.lcs_core > 0]
        else:
            workdf['BC']=workdf.ref_list.apply(lambda x: len([z for z in x if z in compdf.lens_id.to_list()]))
            workdf['lcs_ci']=workdf.apply(lambda row: round(row['BC']/row['scholarly_citations_count'],2) if row['scholarly_citations_count'] != 0 else 0, axis = 1)
            workdf['spscore_ci']=workdf.apply(lambda row: round(row['lcs_ci']/row['scholarly_citations_count'],3) if row['scholarly_citations_count'] != 0  and row['lcs_ci'] !=0 else 0,axis=1 )
    else:
        return workdf
        
    return workdf

def corpus_domain(dataframe):
    '''populate dafaframe with text markers extracted'''
    workdf=dataframe.copy()
    workdf['cited_concepts'] = workdf.text_dat.parallel_apply(lambda x: get_detail(x,True,False))
    workdf['cited_agri'] = workdf.text_dat.parallel_apply(lambda x: get_detail(x,False,True))
    workdf['indomain'] = workdf.parallel_apply(lambda row: 1 if row['cited_concepts'] != 0 and row['cited_agri'] != 0 else 0,axis=1)
    return workdf


def cleaner(txt:str):
    '''cleaner takes raw string as input and return a cleaned string'''
    #### remove sequence of - or = or +
    cleaned=re.sub(r'(?:^|\s)[\-=\+]{2,}(?:\s|$)',' ',txt)
    #### remove url 
    cleaned=re.sub(r'https?://[^\s<>"]+|www\.[^\s<>"]+',' ',cleaned)
    #### remove html tags
    cleaned=re.sub(r'<[^<>]*>','',cleaned)
    #### remove code between brackets
    cleaned=re.sub(r'\[[^\[\]]*\]',' ',cleaned)
    #### remove plural forms between parentheses
    cleaned=re.sub(r'\(s\)',' ',cleaned)
    #### remove parenthereses happening in middle of a word
    cleaned=re.sub(r'[a-z]\(|\)[a-z]',' ',cleaned)
    #### remove i.e or that sort of abbreviations
    cleaned=re.sub(r'asf\.e\.|i\.e',' ',cleaned)
    #### remove some puncutation signs
    cleaned=re.sub(r'!|"|#|\$|%|&|\*|\+|\?|@|_|`|\{|\}|~|:','',cleaned)
    cleaned=re.sub(r'�{1,2}|{1,2}\s{0,1}','',cleaned)
    cleaned=re.sub(r'#\s{0,2}','',cleaned)
    cleaned=re.sub(r'%\s{0,2}%{0,1}','',cleaned)
    #### removing japanese characters
    hiragana = re.compile('[\u3040-\u309F]')
    katakana = re.compile('[\u30A0-\u30FF]')
    CJK = re.compile('[\u4300-\u9faf]')
    chinese= re.compile('[\u4e00-\u9fff]')
    cleaned = hiragana.sub('', cleaned)
    cleaned = katakana.sub('', cleaned)
    cleaned = CJK.sub('', cleaned)
    cleaned = chinese.sub('', cleaned)
    #### removing all others non-latin characters(https://stackoverflow.com/questions/23680976/python-removing-non-latin-characters)
    cleaned=re.sub(r'[^\x00-\x7f]',r'',cleaned)
    cleaned=re.sub('Abstract|Introduction|n/a','',cleaned)
    return cleaned


def custom_tokenizer(nlp):
    ''' this customized tokenizing function is extracted from O'Reilly Book on text mining'''
    # use default patterns except the ones matched by re.search
    prefixes=[pattern for pattern in nlp.Defaults.prefixes if pattern not in ['-','_','#']]
    suffixes=[pattern for pattern in nlp.Defaults.suffixes if pattern not in ['_']]
    infixes=[pattern for pattern in nlp.Defaults.infixes if not re.search(pattern, 'xx-xx')]
    
    return Tokenizer(vocab = nlp.vocab,
                     rules = nlp.Defaults.tokenizer_exceptions,
                     prefix_search = compile_prefix_regex(prefixes).search,
                     suffix_search = compile_suffix_regex(suffixes).search,
                     infix_finditer = compile_infix_regex(infixes).finditer,
                     token_match = nlp.Defaults.token_match)

nlp = spacy.load('en_core_web_lg')

#stop_words = stop_words.STOP_WORDS
nlp.tokenizer = custom_tokenizer(nlp)
matcher = Matcher(nlp.vocab)

patterns = {
    "noun_verb": [{"POS": "NOUN"}, {"POS": "VERB"}],
    "noun_noun": [{"POS": "NOUN"}, {"POS": "NOUN"}],
    "noun_propn":[{"POS": "NOUN"}, {"POS": "PROPN"}],
    "verb_noun": [{"POS": "VERB"}, {"POS": "NOUN"}],
    "adj_noun": [{"POS": "ADJ"}, {"POS": "NOUN"}],
    "adj_propn": [{"POS": "ADJ"}, {"POS": "PROPN"}]}
for pattern_name, pattern in patterns.items():
    matcher.add(pattern_name, [pattern])

def text_extraction(rawstring,bigram=False):
    
    def extract_features(text:str,nochk=False):
        doc=nlp(text)
        extokens=[tk for tk in doc if tk.text not in stop_words and tk.pos_ in ['ADJ','NOUN','VERB','PROPN']]
        if nochk is False:
            return extokens
        else:
            matches = matcher(doc)
            bigramlist=[]
            for match_id, start, end in matches:
                span = doc[start:end]
                bigramlist.append(span.text.lower())
            
        return bigramlist

    cleanedstring=cleaner(rawstring)
    if bigram is False:
        spacytokens=extract_features(cleanedstring)
        raw_tokens=[str(y.text).lower() for y in spacytokens]
        return raw_tokens
    else:
        spacybigrams=extract_features(cleanedstring,True)
        return spacybigrams




def specific(ngramlst):
    vals=[]
    for el in ngramlst:
        if re.search('path',el) and re.search('depend',el):
            vals.append(el)
        elif re.search('lock',el) and not re.search('block|clock|flock|fetlock|lockdown',el):
            vals.append(el)
        elif re.search('incr',el) and re.search('ret',el):
            vals.append(el)
    if len(vals) >=1:
        return vals
    else:
        return 0

def agrispec(ngramlst):
    vals=[]
    for el in ngramlst:
        if re.search('agr(i|o)',el) and not re.search('magr',el):
            vals.append(el)
        elif re.search('food',el) and not re.search('sea',el):
            vals.append(el)
        elif el.startswith('crop') or el.startswith('farm') or el.startswith('livestock') or el.startswith('dairy') or el.startswith('fisher'):
            vals.append(el)    
    if len(vals) >=1:
        return vals
    else:
        return 0







def in_domain(title_abstract:string,get_extract=False):
    '''function returns if publication is in domain or not'''
    txt_tks = text_extraction(title_abstract)
    txt_big = text_extraction(title_abstract,True)
    unigram_concept = specific(txt_tks)
    unigram_agri = agrispec(txt_tks)
    bigram_concept = specific(txt_big)
    bigram_agri = agrispec(txt_big)

    if get_extract is False:
        if unigram_concept != 0  or bigram_concept != 0:
            if unigram_agri != 0 or bigram_agri != 0:
                return 1
            else:
                return 0
        else:
            return 0
    else:
        if unigram_concept != 0  or bigram_concept != 0:
            if unigram_agri != 0 or bigram_agri != 0:
                return([[unigram_concept+bigram_concept],[unigram_agri+bigram_agri]])
            else:
                return 0
        else:
            return 0



def get_detail(title_abstract,conc=False, agri=False):
    '''function returns if publication is in domain or not'''
    txt_tks = text_extraction(title_abstract)
    txt_big = text_extraction(title_abstract,True)
    unigram_concept = specific(txt_tks)
    unigram_agri = agrispec(txt_tks)
    bigram_concept = specific(txt_big)
    bigram_agri = agrispec(txt_big)

    if conc is True:
        retrieved=[]
        if unigram_concept != 0:
            for val in unigram_concept:
                retrieved.append(val)
        if bigram_concept != 0:
            for val in bigram_concept:
                retrieved.append(val)
        if len(retrieved) != 0:
            return list(np.unique(retrieved))
        else:
            return 0
    
    if agri is True:
        retrieved=[]
        if unigram_agri != 0:
            for val in unigram_agri:
                retrieved.append(val)
        if bigram_agri != 0:
            for val in bigram_agri:
                retrieved.append(val)
        if len(retrieved) != 0:
            return list(np.unique(retrieved))
        else:
            return 0


# Bibliographic platform functions

def get_lensmeta(idlist):
    def chunk(it, size):
        it = iter(it)
        return iter(lambda: tuple(itertools.islice(it, size)), ())
    
    respres=[]
    
    sublists=list(chunk(idlist,500))
    sublists=[list(x) for x in sublists]

    url = 'https://api.lens.org/scholarly/search'

    include = '''["lens_id",
        "external_ids",
        "date_published_parts",
        "authors.last_name",
        "authors.first_name",
        "author_count",
        "title",
        "publication_type",
        "source",
        "fields_of_study",
        "abstract",
        "scholarly_citations_count",
        "references",
        "references_count"]'''

    for sub in sublists:
        data = '''{
            "query": {
            "terms": {
                "lens_id": '''+re.sub('\'','"',str(sub))+'''    }
            },
            "size": 500,
            "include": %s,
            
            "scroll": "1m"
            }'''% include
        headers = {'Authorization': 'UxbMn8hMdGqGWDjnXKxYs9tY7Ecf6iurtGqh3iFt9Dz4epCtTKnm', 'Content-Type': 'application/json'}
        response = requests.post(url, data=data, headers=headers)
        if response.status_code == requests.codes.ok:
            myjson = response.json()
            print(myjson['total'])
            respres.append(myjson['data'])
            time.sleep(5)
        else:
            print(print(response.json()))
            break

    return respres


def get_lensciting(lens_id):

    def query_format(lensid,metadata=False,start=1,size=1000):
        if metadata is False:
            query='''{
                    "query": {
                        "match": {
                        "reference.lens_id": '''+'"'+lensid+'"'+'''
                        }
                    },
                    "from":'''+str(start)+''',
                    "size":'''+str(size)+'''
                    }'''
            return query
        else :
            query='''{
                    "query": {
                        "match": {
                        "reference.lens_id": '''+'"'+lensid+'"'+'''
                        }
                    },
                    "include":'''+metadata+''',
                    "from":'''+str(start)+''',
                    "size":'''+str(size)+'''
                    }'''
            return query
    
    def test_response(api_url,api_query,api_headers):
        api_resp = requests.post(url=api_url, data=api_query, headers=api_headers)
        resp_code = api_resp.status_code
        if resp_code == requests.codes.ok:
            return (True,api_resp.json()['total'])
        else:
            return api_resp.json()
    
    
    url = 'https://api.lens.org/scholarly/search'
    
    headers = {'Authorization': 'UxbMn8hMdGqGWDjnXKxYs9tY7Ecf6iurtGqh3iFt9Dz4epCtTKnm', 'Content-Type': 'application/json'}
    
    include = '''["lens_id",
        "external_ids",
        "date_published_parts",
        "authors.last_name",
        "authors.first_name",
        "author_count",
        "title",
        "publication_type",
        "source",
        "fields_of_study",
        "abstract",
        "scholarly_citations_count",
        "references",
        "references_count"]'''

    retrieved_dat=[]
    base=query_format(lens_id)
    test_id=test_response(url,base,headers)
    if test_id[0] is True and test_id[1] <= 1000:
        time.sleep(3)
        data=requests.post(url, data=query_format(lens_id,include), headers=headers).json()
        retrieved_dat.append(data['data'])
    elif test_id[0] is True and test_id[1] > 1000:
        total=range(test_id[1])
        chunks=[total[i:i+1000] for i in range(0, len(total), 1000)]
        for chk in chunks:
            data=requests.post(url, data=query_format(lens_id,include,chk[0],1000), headers=headers).json()
            retrieved_dat.append(data['data'])
            time.sleep(3)
    else:
        print(test_id)
    return retrieved_dat


def json_parser(jsondict):
    '''blabla blabla blablablabla'''

    def authoriter(authdict):
        firstname='n/a'
        lastname='n/a'
        if 'first_name' in authdict.keys():
            firstname=authdict['first_name']
        if 'last_name' in authdict.keys():
            lastname=authdict['last_name']
        
        if firstname != 'n/a' and lastname != 'n/a':
            return str(firstname+' '+lastname)
        elif firstname != 'n/a' and lastname == 'n/a':
            return str(firstname)
        elif firstname == 'n/a' and lastname != 'n/a':
            return str(lastname)
        else:
            return 'n/a'
        
    normdict={}
    
    ### the first loop return a copy of the original json dict with 'n/a' value for missing keys
    strgroup=['lens_id','title','publication_type','abstract']
    listgroup=['date_published_parts','external_ids','authors','fields_of_study','reference']
    intgroup=['references_count','scholarly_citations_count','author_count']
    dictgroup=['source']

    for exp in strgroup:
        if exp in jsondict.keys():
            normdict[exp]=jsondict[exp]
        else:
            normdict[exp]='n/a'

    for exp in intgroup:
        if exp in jsondict.keys():
            normdict[exp]=jsondict[exp]
        else:
            normdict[exp]=0
    
    

    if 'date_published_parts' in jsondict.keys():
        if len(jsondict['date_published_parts']) == 3:
            normdict['year']=jsondict['date_published_parts'][0]
            normdict['month']=jsondict['date_published_parts'][1]
            normdict['day']=jsondict['date_published_parts'][2]
        elif len(jsondict['date_published_parts']) == 2:
            normdict['year']=jsondict['date_published_parts'][0]
            normdict['month']=jsondict['date_published_parts'][1]
            normdict['day']='n/a'
        elif len(jsondict['date_published_parts']) == 1:
            normdict['year']=jsondict['date_published_parts'][0]
            normdict['month']='n/a'
            normdict['day']='n/a'
    else:
        normdict['year']='n/a'
        normdict['month']='n/a'
        normdict['day']='n/a'
    
    if 'external_ids' in jsondict.keys():
        extract=[el['value'] for el in jsondict['external_ids'] if el['type'] == 'doi' ]
        if len(extract) >=1:
            normdict['doi']=extract[0]
        else:
            normdict['doi']='n/a'
    else:
        normdict['doi']='n/a'
    
    if 'authors' in jsondict.keys():
        retrievedauth=[authoriter(el) for el in jsondict['authors']]
        normdict['authors'] = ';'.join(retrievedauth)
    else:
        normdict['authors'] = 'n/a'
    
    if 'fields_of_study' in jsondict.keys():
        normdict['fields']=';'.join(jsondict['fields_of_study'])
    else:
        normdict['fields']='n/a'
    
    if 'references' in jsondict.keys():
        normdict['references']=';'.join([el['lens_id'] for el in jsondict['references']])
    else:
        normdict['references']='n/a'
    
    if 'source' in jsondict.keys():
        if 'title' in jsondict['source'].keys():
            normdict['source_title']=jsondict['source']['title']
        else:
            normdict['source_title']='n/a'
        if 'asjc_subjects' in jsondict['source'].keys():
            normdict['source_fields']=';'.join(jsondict['source']['asjc_subjects'])
        else:
            normdict['source_fields']='n/a'
    else:
        normdict['source_title']='n/a'
        normdict['source_fields']='n/a'
    return normdict

def lens_parser(dictlist:list):
    parsed=[]
    for s,sets in enumerate(dictlist):
        for i,el in enumerate(sets):
            try:
                res=json_parser(el)
                parsed.append(pd.DataFrame(res,index=[0]))
            except:
                print(s,i)
                break
    concatdfs=pd.concat(parsed).reset_index()
    return concatdfs

### Function for metrics
def ncs(reflista,reflistb):
    '''NCS stands for normalised coupling strength. Function returns the ncs of two publications. For formulae see: https://doi.org/10.1016/j.joi.2007.07.004'''
    interef=len(list(set(reflista) & set(reflistb)))
    refprod=len(reflista)*len(reflistb)
    divider=pow(refprod,1/2)
    return interef/divider

def get_distrib(dataframe, col, withgraph=False):
    
    eff_tble=dataframe.groupby(col).size().to_frame('Effectifs').sort_values(by=col,ascending=False).reset_index()
    eff_tble['Effectifs cumulés']=eff_tble.Effectifs.cumsum()
    eff_tble['%_effectifs']=eff_tble['Effectifs cumulés'].apply(lambda x: round((x/eff_tble.Effectifs.sum())*100,0))
    if withgraph is True:
        tempserie=dataframe[[col]].sort_values(by=col,ascending=True).reset_index()
        tempserie['idx']=tempserie.index
        plt.plot(tempserie.idx.to_list(), tempserie[col].to_list())
        plt.savefig(str(col)+'_Distribution.png')
        plt.show()
        plt.boxplot(tempserie[col])
        plt.savefig(str(col)+'_Boxplot.png')
        plt.show()
        
        #print(eff_tble.boxplot(column=col))
    return eff_tble.style.format_index(escape="latex")

def set_ncs(finalcorp,corpcore,overlap=False,getoverlap=False):
    if overlap is False:
        finalcorp_nocore=finalcorp[~finalcorp.lens_id.isin(corpcore.lens_id.to_list())]
        all_final_refs=list(dict.fromkeys(ref for x in finalcorp_nocore.ref_list.to_list() for ref in x))
        all_core_ref=list(dict.fromkeys(ref for x in corpcore.ref_list.to_list() for ref in x))
        return round(ncs(all_final_refs,all_core_ref),3)
    else:
        if getoverlap is True:
            overlap=len([x for x in finalcorp.lens_id.to_list() if x in corpcore.lens_id.to_list()])
            return overlap
        else:
            all_final_refs=list(dict.fromkeys(ref for x in finalcorp.ref_list.to_list() for ref in x))
            all_core_ref=list(dict.fromkeys(ref for x in corpcore.ref_list.to_list() for ref in x))
            return round(ncs(all_final_refs,all_core_ref),3)